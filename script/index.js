const validateNumber = number => (number !== null && number !== '' && !isNaN(number));
const getNumber = (message = 'Enter your number') => {
    let userNumber;

    do {
        userNumber = prompt(message);
    } while (!validateNumber(userNumber));

    return +userNumber;
}

const calcResult = (mathOperation) => {

    const a = getNumber('Enter first number');
    const b = getNumber('Enter second number');

    switch (mathOperation) {

        case '+': {
            return a + b;
        }

        case '-': {
            return a - b;
        }

        case '*': {
            return a * b;
        }

        case '/': {
            return a / b;
        }
    }
}

let mathOperator;

do {
    mathOperator = prompt('Enter mathematical operator: +, -, *, /');

} while (mathOperator !== '+' && mathOperator !== '-' && mathOperator !== '*' && mathOperator !== '/');

console.log(calcResult(mathOperator));

//более простой вариант калькулятора:
/*

let numberOne = +prompt('Enter Number One');
let operator = prompt('Enter operator');
let numberTwo = +prompt('Enter Number Two');

function calculator(a,b,c){
   
    if(b === '+'){

       return   a + c;

    } else if( b === '-'){

        return   a - c;

    } else if(b === '*'){

        return   a * c;

    }else if(b === '/'){

        return   a / c;

    }

}

calculator();

console.log(calculator(numberOne, operator,numberTwo));

*/

/*

1.Описати своїми словами навіщо потрібні функції у програмуванні.

Функція потрібна для спрощення кодування, завдяки функції ми можемо не повторювати код, а написати один раз і в подальшому 
викликати її в будь якому місті.

2.Описати своїми словами, навіщо у функцію передавати аргумент.

Аргументи задаються для того, щоб потім, при виклику функції можна було туди покласти
локальні змінні, і вони вже будуть застосовані у тілі функції. 


3.Що таке оператор return та як він працює всередині функції?

Якщо функція має обробити якісь дані і потім повернути їх, то для повернення даних необхідний оператор return. 
Return повертає нам значення, як тільки код доходить до return, функція зупиняється,
і нам повертається з неї значення.

*/